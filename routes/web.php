<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(session('token'))
        return redirect('branch');
        
    return view('welcome');
});

Route::get('/logout', function(){
    $token = session('token');
    session()->forget('token');
    session()->flush();
    return redirect()->to(env('BKM_URL_LOGOUT') . "?token=" . $token . "&code=" . env('BKM_CODE'));
})->name('logout');

Route::post('/login', 'LoginController@login')->name('login');
Route::get('/login', 'LoginController@receiver')->name('login.receiver');

Route::group(['middleware' => 'auth.custom'], function(){

    Route::group(['prefix' => 'branch'], function(){

        Route::get('/', 'BranchController@index')->name('branch');
        Route::post('/detail', 'BranchController@detail')->name('branch.detail');

        Route::get('/add', 'BranchController@add')->name('branch.add');
        Route::post('/add', 'BranchController@store')->name('branch.store');

        Route::get('/{id}', 'BranchController@edit')->name('branch.edit');
        Route::put('/{id}', 'BranchController@update')->name('branch.update');

        Route::delete('/{id}', 'BranchController@delete')->name('branch.delete');

    });

    Route::group(['prefix' => 'service'], function(){

        Route::get('/', 'ServiceController@index')->name('service');

        Route::get('/add', 'ServiceController@add')->name('service.add');
        Route::post('/add', 'ServiceController@store')->name('service.store');

        Route::get('/{id}', 'ServiceController@edit')->name('service.edit');
        Route::put('/{id}', 'ServiceController@update')->name('service.update');

        Route::delete('/{id}', 'ServiceController@delete')->name('service.delete');

    });

    Route::group(['prefix' => 'transaction'], function(){

        Route::get('/', 'TransactionController@index')->name('transaction');
        Route::get('/{id}/detail', 'TransactionController@detail')->name('transaction.detail');
        Route::get('/{id}/print', 'TransactionController@print')->name('transaction.print');

        Route::get('/add', 'TransactionController@add')->name('transaction.add');
        Route::post('/add', 'TransactionController@store')->name('transaction.store');

        Route::put('/{id}/cancel', 'TransactionController@cancel')->name('transaction.cancel');
        Route::put('/{id}/finish', 'TransactionController@finish')->name('transaction.finish');

    });

    Route::get('/report', function(){
        return view('transaction.report');
    })->name('report');
    Route::get('/report/preview', 'TransactionController@report')->name('report.preview');

});