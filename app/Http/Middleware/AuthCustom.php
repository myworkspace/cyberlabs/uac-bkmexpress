<?php

namespace App\Http\Middleware;

use Closure;

class AuthCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->session()->has('token')) {
            return redirect('/');
        } else {
            // get user data
            $params['token'] = $request->session()->get('token');
            $user = curl(env('BKM_URL_DATA'), $params);

            if($user == null){
                return redirect('logout');
            } else {
                $request->request->add([
                    'user' => [
                        'email' => $user->email,
                        'name' => $user->name,
                        'level' => $user->level_id,
                    ],
                ]);

                if(!isAllowed($request->route()->getName(), $user->level_id))
                    abort(404);
            }
        }
        return $next($request);
    }
}
