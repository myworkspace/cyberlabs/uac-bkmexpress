<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Service;
use Validator;

class TransactionController extends Controller
{
    
    public function index(Request $request){
        $page = $request->input('page');
        $limit = 15;
        $num = 0;
        if($page > 0) $num = ($page - 1) * $limit;

        $appends = [];
        $query = Transaction::getHistory();

        $filter = $request->input('filter');
        if($filter >= 0 && $filter != ''){
            $query->where('status', '=', $filter);
            $appends['filter'] = $filter;
        }

        $search = $request->input('search');
        if($search){
            $query->where(function($query) use ($search) {
                $query->where('invoice', 'LIKE', '%' . $search . '%')
                    ->orWhere('branches.city', 'LIKE', '%' . $search . '%')
                    ->orWhere('customer_name', 'LIKE', '%' . $search . '%');
            });
            $appends['search'] = $search;
        }

        $transactions = $query->orderBy('id', 'desc')->paginate($limit)->appends($appends);

        return view('transaction.index', [
            'transactions' => $transactions,
            'num' => $num,
        ]);
    }

    private function getLastInvoice(){

        $date = date('y/m/d');

        $transaction = Transaction::where('invoice', 'LIKE', $date . '%')->orderBy('id', 'desc')->first();
        if($transaction){
            $id = $transaction->id;
        } else {
            $id = 0;
        }

        $id++;
        $number = str_pad($id, 4, '0', STR_PAD_LEFT);
        $invoice = '#' . date('y/m/d') . '/' . $number;

        return $invoice;
    }

    public function add(){

        $invoice = $this->getLastInvoice();

        $title = __('menus.transaction') . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.add');
        $date = date('d-m-Y H:i:s');

        $services = Service::get();

        return view('transaction.form', [
            'title' => $title,
            'invoice' => $invoice,
            'date' => $date,
            'services' => $services,
        ]);

    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'branch' => 'required',
            'customer_name' => 'required',
            'customer_phone' => 'required',
            'customer_address' => 'required',
            'receiver_name' => 'required',
            'receiver_phone' => 'required',
            'receiver_address' => 'required',
            'total' => 'required',
            'due' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('transaction.add')
                    ->withErrors($validator)
                    ->withInput();
        }

        // update invoice
        $invoice = $this->getLastInvoice();

        $create = [
            'invoice' => $invoice,
            'user_email' => $request->user['email'],
            'user_name' => $request->user['name'],
            'branch_id' => $request->input('branch'),
            'customer_name' => $request->input('customer_name'),
            'customer_phone' => $request->input('customer_phone'),
            'customer_address' => $request->input('customer_address'),
            'receiver_name' => $request->input('receiver_name'),
            'receiver_phone' => $request->input('receiver_phone'),
            'receiver_address' => $request->input('receiver_address'),
            'total' => $request->input('total'),
            'due' => $request->input('due'),
        ];

        $transaction_id = Transaction::create($create)->id;

        $services = $request->input('service');
        $descriptions = $request->input('description');
        $quantities = $request->input('quantity');
        $weights = $request->input('weight');
        $prices = $request->input('price');
        $durations = $request->input('duration');

        foreach($services as $key => $service){
            $description = $descriptions[$key];
            $quantity = $quantities[$key];
            $weight = $weights[$key];
            $price = $prices[$key];
            $duration = $durations[$key];

            if($service && $description && $quantity > 0 && $weight > 0 && $price && $duration > 0){

                $create = [
                    'transaction_id' => $transaction_id,
                    'service_id' => $service,
                    'description' => $description,
                    'quantity' => $quantity,
                    'weight' => $weight,
                    'price' => $price,
                    'duration' => $duration,
                ];

                TransactionDetail::create($create);

            }
        }

        return redirect()
                ->route('transaction.detail', ['id' => $transaction_id])
                ->withSuccess(__('alert.message.store', ['attribute' => __('menus.transaction')]));

    }

    public function detail($id){

        $title = __('menus.transaction') . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.detail');

        $transaction = Transaction::with(['branch', 'transaction_detail'])->findOrFail($id);
        $num = 0;

        return view('transaction.detail', [
            'transaction' => $transaction,
            'title' => $title,
            'num' => $num,
        ]);

    }

    public function print($id){

        $title = __('menus.transaction') . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.detail');

        $transaction = Transaction::with(['branch', 'transaction_detail'])->findOrFail($id);
        $num = 0;

        return view('transaction.invoice', [
            'transaction' => $transaction,
            'title' => $title,
            'num' => $num,
        ]);

    }

    public function cancel($id){

        $update = ['status' => 0];
        Transaction::where('id', $id)->update($update);

        return back()
            ->withSuccess(__('alert.message.cancel', ['attribute' => __('menus.transaction')]));

    }

    public function finish($id){

        $update = ['status' => 2];
        Transaction::where('id', $id)->update($update);

        return back()
            ->withSuccess(__('alert.message.finish', ['attribute' => __('menus.transaction')]));

    }

    public function report(Request $request){
        
        $start = $request->input('start');
        $end = $request->input('end');

        $page = $request->input('page');
        $limit = 15;
        $num = 0;
        if($page > 0) $num = ($page - 1) * $limit;

        $appends = [
            'start' => $start,
            'end' => $end,
        ];

        $where = function($query) use ($start, $end){
            $query->where('transactions.created_at', '>=', $start . ' 00:00:00')
                    ->where('transactions.created_at', '<=', $end . ' 24:00:00');
        };

        $transactions = Transaction::getHistory()
                    ->where($where)
                    ->orderBy('created_at', 'asc')
                    ->paginate($limit)
                    ->appends($appends);

        $grand_total = Transaction::getGrandTotal()
                    ->where($where)
                    ->first()->grand_total;

        return view('transaction.preview', [
            'transactions' => $transactions,
            'start' => $start,
            'end' => $end,
            'num' => $num,
            'grand_total' => $grand_total,
        ]);
        

    }

}
