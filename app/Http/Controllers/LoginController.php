<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    
    public function login(){

        $raw = env('BKM_CODE') . env('BKM_SECRET_KEY') . 'request' . date('Ymd');
        $key = hash('sha1', $raw);

        return redirect()->to(env('BKM_URL') . '?code=' . env('BKM_CODE') . '&key=' . $key);
    }

    public function receiver(Request $request){
        $raw = env('BKM_CODE') . env('BKM_SECRET_KEY') . 'accepted' . date('Ymd');
        $app_key = hash('sha1', $raw);
    
        if($app_key == $request->input('key') && $request->input('token')){
            session(['token' => $request->input('token')]);
            return redirect()->route('transaction');
        } else {
            return redirect('/');
        }
    }

}
