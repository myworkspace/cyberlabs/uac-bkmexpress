<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Helpers\CommonHelper;
use Validator;

class ServiceController extends Controller
{
    
    public function index(){

        $num = 0;
        $services = Service::get();
        return view('service.index', [
            'services' => $services,
            'num' => $num,
        ]);

    }

    public function add(){

        $title = __('menus.service') . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.add');
        return view('service.form', [
            'title' => $title,
        ]);

    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'duration' => 'required|gt:1',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('service.add')
                    ->withErrors($validator)
                    ->withInput();
        }

        $create = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'duration' => $request->input('duration'),
        ];

        Service::create($create);

        return redirect()
                ->route('service')
                ->withSuccess(__('alert.message.store', ['attribute' => __('menus.service')]));

    }

    public function edit($id){

        $title = __('menus.service') . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.edit');

        $service = Service::findOrFail($id);

        return view('service.form', [
            'title' => $title,
            'service' => $service,
            'edit' => true,
        ]);

    }

    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'duration' => 'required|gt:1',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('service.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
        }

        $update = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'duration' => $request->input('duration'),
        ];

        Service::where('id', $id)->update($update);

        return redirect()
                ->route('service')
                ->withSuccess(__('alert.message.update', ['attribute' => __('menus.service')]));

    }

    public function delete($id){

        try {
            Service::where('id', $id)->delete();
            return back()
                ->withSuccess(__('alert.message.delete', ['attribute' => __('menus.service')]));
        }
        catch (\Exception $e) {
            return back()
                ->withError(__('alert.error.delete', ['attribute' => __('menus.service')]));
        }

    }

}
