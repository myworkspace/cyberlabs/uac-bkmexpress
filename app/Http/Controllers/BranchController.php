<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branch;
use Validator;
use App\Helpers\CommonHelper;

class BranchController extends Controller
{

    public function index(Request $request){

        $page = $request->input('page');
        $limit = 10;
        $num = 0;
        if($page > 0) $num = ($page - 1) * $limit;

        $appends = [];
        $query = Branch::orderBy('id', 'desc');

        $filter = $request->input('filter');
        if($filter >= 0 && $filter != ''){
            $query->where('type', '=', $filter);
            $appends['filter'] = $filter;
        }

        $search = $request->input('search');
        if($search){
            $query->where(function($query) use ($search) {
                $query->where('city', 'LIKE', '%' . $search . '%');
            });
            $appends = ['search', $search];
        }

        $branches = $query->paginate($limit)->appends($appends);

        return formatResponse($request, [
            'branches' => $branches,
            'num' => $num,
        ], 'branch.index');
    }

    public function add(){
        $title = __('menus.branch') . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.add');
        $types = config('constant.branch.types');
        return view('branch.form', [
            'title' => $title,
            'types' => $types,
            'type_id' => 1,
        ]);
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'city' => 'required',
            'type' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'phone' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('branch.add')
                    ->withErrors($validator)
                    ->withInput();
        }

        $create = [
            'city' => $request->input('city'),
            'type' => $request->input('type'),
            'address' => $request->input('address'),
            'contact' => $request->input('contact'),
            'code' => $request->input('code'),
            'phone' => $request->input('phone'),
            'fax' => $request->input('fax'),
            'cellular' => $request->input('celullar'),
            'email' => $request->input('email'),
        ];

        Branch::create($create);

        return redirect()
                ->route('branch')
                ->withSuccess(__('alert.message.store', ['attribute' => __('menus.branch')]));

    }

    public function edit($id){

        $branch = Branch::findOrFail($id);
        $title = __('menus.branch') . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.edit');
        $types = config('constant.branch.types');
        return view('branch.form', [
            'edit' => true,
            'title' => $title,
            'types' => $types,
            'type_id' => $branch->type,
            'branch' => $branch,
        ]);

    }

    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'city' => 'required',
            'type' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'phone' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('branch.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput();
        }

        $create = [
            'city' => $request->input('city'),
            'type' => $request->input('type'),
            'address' => $request->input('address'),
            'contact' => $request->input('contact'),
            'code' => $request->input('code'),
            'phone' => $request->input('phone'),
            'fax' => $request->input('fax'),
            'cellular' => $request->input('celullar'),
            'email' => $request->input('email'),
        ];

        Branch::where('id', $id)->update($create);

        return redirect()
                ->route('branch')
                ->withSuccess(__('alert.message.update', ['attribute' => __('menus.branch')]));

    }

    public function delete($id){

        try {
            Branch::where('id', $id)->delete();
            return back()
                ->withSuccess(__('alert.message.delete', ['attribute' => __('menus.branch')]));
        }
        catch (\Exception $e) {
            return back()
                ->withError(__('alert.error.delete', ['attribute' => __('menus.branch')]));
        }

    }

    public function detail(Request $request){

        $id = $request->input('id');

        $branch = Branch::findOrFail($id);
        return jsonResponse($branch);

    }

}
