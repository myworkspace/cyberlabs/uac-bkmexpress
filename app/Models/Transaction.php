<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Transaction extends Model
{
    
    protected $fillable = [
        'invoice', 
        'user_email', 
        'user_name', 
        'branch_id',
        'customer_name',
        'customer_phone',
        'customer_address',
        'receiver_name',
        'receiver_phone',
        'receiver_address',
        'due',
        'total',
    ];

    protected $dates = [
        'due',
    ];

    protected $appends = [
        'status_name'
    ];

    public function getStatusNameAttribute(){
        return __('pages.transaction.fields.status.' . config('constant.transaction.status.' . $this->status));
    }

    public function scopeGetHistory($query){

        $fields = DB::raw('transactions.*, branches.city AS branch_city, branches.address AS branch_address, branches.phone AS branch_phone');

        $result = $query->select($fields)
                            ->join('branches', 'branches.id', '=', 'transactions.branch_id');
        
        return $result;

    }

    public function scopeGetGrandTotal($query){
        $fields = DB::raw('SUM(total) AS grand_total');
        $result = $query->select($fields)
                    ->join('branches', 'branches.id', '=', 'transactions.branch_id');

        return $result;
    }

    public function branch(){
        return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
    }

    public function transaction_detail(){
        return $this->hasMany('App\Models\TransactionDetail', 'transaction_id', 'id');
    }

}
