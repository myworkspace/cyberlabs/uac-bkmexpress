<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    
    protected $fillable = [
        'transaction_id', 
        'service_id', 
        'description',
        'quantity',
        'weight',
        'price',
        'duration',
    ];

    public function transaction(){
        return $this->belongsTo('App\Models\Transaction', 'transaction_id', 'id');
    }
    
    public function service(){
        return $this->belongsTo('App\Models\Service', 'service_id', 'id');
    }

}
