<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{

    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name', 
        'price', 
        'duration',
    ];

    public function transaction_detail(){
        return $this->hasMany('App\Models\TransactionDetail', 'transaction_id', 'id');
    }

}
