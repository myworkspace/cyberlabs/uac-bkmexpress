<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{

    use SoftDeletes;

    protected $table = 'branches';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'city', 
        'type', 
        'address', 
        'code',
        'phone',
        'fax',
        'contact',
        'cellular',
        'email',
    ];

    protected $appends = [
        'type_name'
    ];

    public function getTypeNameAttribute(){
        return config('constant.branch.types.' . $this->type);
    }

    public function transactions(){
        return $this->hasMany('App\Models\Transaction', 'branch_id', 'id');
    }

}
