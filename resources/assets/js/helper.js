$(document).ready(function(){

    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

});

function submitForm(formId, buttonId, confirmation){

    formId = formId ? formId : '#form';
    buttonId = buttonId ? buttonId : '#button';

    var cont = true;
    if(confirmation)
        cont = confirm(confirmation);

    if(cont == true){
        
        $(buttonId).prop('disabled', 'disabled');
        $(buttonId).html('Loading..');

        $(formId).submit();
    }

}

function getData(end_point, params, onDone, onFail){

    var request;

    request = $.ajax({
        url: end_point,
        method: "POST",
        data: params,
        datatype: "json"
    });

    if(!onDone){
        onDone = function(result) {
            return result;
        }
    }

    request.done(onDone);

    if(!onFail){
        onFail = function(jqXHR, textStatus) {
            alert('Something when wrong');
            console.log(textStatus);
        }
    }

    request.fail(onFail);

}

Number.prototype.formatCurrency = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 0 : c, 
    d = d == undefined ? "," : d, 
    t = t == undefined ? "." : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

 Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf());
    dat.setDate(eval(dat.getDate()) + eval(days));
    return dat;
}

function formatDate(date, long) {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
  
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
  
    if(long == true){
        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    } else {
        monthIndex++;
        return year + '-' + (monthIndex < 10 ? '0' : '') + monthIndex + '-' + (day < 10 ? '0' : '') + day;
    }
  }