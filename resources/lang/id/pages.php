<?php

return [

    'optional' => 'Opsional',
    'searching' => 'Pencarian',
    'filter' => 'Filter',

    'branch' => [
        'title' => 'Cabang',
        'fields' => [
            'city' => 'Kota',
            'type' => 'Jenis',
            'address' => 'Alamat',
            'code' => 'Kode Area',
            'phone' => 'Telepon',
            'fax' => 'Fax',
            'contact' => 'Kontak',
            'cellular' => 'HP',
            'email' => 'Email',
            'action' => 'Aksi',
        ],
    ],

    'service' => [
        'title' => 'Layanan',
        'fields' => [
            'name' => 'Nama',
            'price' => 'Harga Per Kg',
            'duration' => 'Maks. Hari',
            'day' => 'Hari',
            'action' => 'Aksi',
        ],
    ],

    'transaction' => [
        'title' => 'Transaksi',
        'fields' => [
            'invoice' => 'Invoice',
            'date' => 'Tanggal',
            'customer' => 'Konsumen',
            'receiver' => 'Penerima',
            'data' => [
                'name' => 'Nama',
                'phone' => 'Telepon',
                'address' => 'Alamat',
            ],
            'user' => 'Pengguna',
            'due' => 'Jatuh Tempo',
            'total' => 'Total',
            'sub_total' => 'Sub Total',
            'grand_total' => 'Grand Total',
            'branch' => 'Cabang',

            'detail' => [
                'service' => 'Layanan',
                'quantity' => 'Jumlah',
                'weight' => 'Berat (Kg)',
                'price' => 'Harga',
                'sub_total' => 'Sub Total',
            ],
            'action' => 'Aksi',
            'status' => [
                'name' => 'Status',
                'cancelled' => 'Dibatalkan',
                'active' => 'Aktif',
                'finished' => 'Selesai',
            ],
        ],
        'message' => [
            'form' => '* Akan diperbarui secara otomatis',
        ]
    ],

    'report' => [
        'title' => 'Laporan',
        'periode' => 'Periode',
        'transaction' => 'Laporan Transaksi',
        'fields' => [
            'start_date' => 'Dari Tanggal',
            'end_date' => 'Sampai Tanggal',
        ],
    ],

];