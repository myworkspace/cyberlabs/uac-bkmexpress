<?php

return [
    'management' => 'MANAJEMEN',
    'branch' => 'Cabang',
    'service' => 'Layanan',
    'transaction' => 'Transaksi',
    'report' => 'Laporan',

    'account' => 'AKUN',
    'logout' => 'Keluar',
];
