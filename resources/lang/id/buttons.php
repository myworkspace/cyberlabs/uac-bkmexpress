<?php

return [

    'login' => 'Login',
    'add' => 'Tambah',
    'add_row' => 'Tambah Baris',
    'edit' => 'Sunting',
    'delete' => 'Hapus',
    'update' => 'Perbarui',
    'save' => 'Simpan',
    'detail' => 'Detail',
    'cancel' => 'Batal',
    'sent' => 'Terkirim',
    'finish' => 'Selesai',
    'back' => 'Kembali',
    'print' => 'Cetak',
    'preview' => 'Pratinjau',
    
];