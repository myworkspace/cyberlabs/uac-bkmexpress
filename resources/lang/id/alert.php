<?php

return [
    'confirmation' => [
        'delete' => 'Anda yakin menghapus :attribute ?',
        'sure' => 'Anda yakin? Hal ini tidak bisa dikembalikan.',
        'logout' => 'Anda yakin keluar?',
    ],

    'message' => [
        'store' => 'Data :attribute berhasil ditambahkan.',
        'update' => 'Data :attribute berhasil diperbarui.',
        'delete' => 'Data :attribute berhasil dihapus.',
        'minimal' => 'Minimal harus ada :number data :attribute',
        'cancel' => 'Data :attribute berhasil dibatalkan.',
        'finish' => 'Data :attribute telah selesai.',
    ],

    'error' => [
        'delete' => 'Data :attribute sudah digunakan.',
    ],
];