@extends('layouts.app')

@section('body')
<form method="post" action="" id="form">

    @csrf

    <div class="row mb-4">
        <div class="col-12">
            <img src="{{ asset('images/logo-small.png') }}" />
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="invoice">@lang('pages.transaction.fields.invoice')</label>
                <h6>{{ $transaction->invoice }} ({{ $transaction->status_name }})</h6>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="date">@lang('pages.transaction.fields.date')</label>
                <h6>{{ $transaction->created_at->format('d-m-Y H:i:s') }}</h6>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="branch">@lang('pages.transaction.fields.branch')</label>
                <h6>{{ $transaction->branch->city }}</h6>
            </div>
        </div>
        <div class="col-sm-6 offset-sm-1">
            <div class="form-group">
                <label>@lang('pages.transaction.fields.data.address')</label>
                <h6>{{ $transaction->branch->address }}</h6>
            </div>
        </div>
    </div>

    <div id="detail" class="table-responsive mt-3 mb-4">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">@lang('pages.transaction.fields.detail.service')</th>
                    <th scope="col" class="text-right">@lang('pages.transaction.fields.detail.weight')</th>
                    <th scope="col" class="text-right">@lang('pages.transaction.fields.detail.quantity')</th>
                    <th scope="col" class="column-action-2 text-right">@lang('pages.transaction.fields.detail.sub_total')</th>
                </tr>
            </thead>
            <tbody>
                @foreach($transaction->transaction_detail as $detail)
                <tr>
                    <td>{{ ++$num }}</td>
                    <td>{{ $detail->description . ' - ' . $detail->duration . ' ' . __('pages.service.fields.day') . ' - ' . formatCurrency($detail->price) }}</td>
                    <td class="text-right">{{ $detail->weight }}</td>
                    <td class="text-right">{{ $detail->quantity }}</td>
                    <td class="text-right">{{ formatCurrency($detail->price * $detail->weight * $detail->quantity) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="row mb-3">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="due">@lang('pages.transaction.fields.due')</label>
                <h6>{{ $transaction->due->format('d-m-Y') }}</h6>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="total">@lang('pages.transaction.fields.total')</label>
                <h6>{{ formatCurrency($transaction->total) }}</h6>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="customer_name">@lang('pages.transaction.fields.customer')</label>
                <h6>{{ $transaction->customer_name }}</h6>
                <h6>{{ $transaction->customer_phone }}</h6>
                <h6>{{ $transaction->customer_address }}</h6>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="receiver_name">@lang('pages.transaction.fields.receiver')</label>
                <h6>{{ $transaction->receiver_name }}</h6>
                <h6>{{ $transaction->receiver_phone }}</h6>
                <h6>{{ $transaction->receiver_address }}</h6>
            </div>
        </div>
    </div>

</form>
@endsection

@section('javascript')
<script>
    window.print();
</script>
@endsection