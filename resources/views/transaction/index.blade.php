@extends('layouts.dashboard')

@section('content')

<div class="row mb-3">
    <div class="col-lg-10">
        <h2>@lang('pages.transaction.title')</h2>
    </div>
	@if(isAllowed('transaction.add', request()->user['level']))
    <div class="col-lg-2 text-lg-right mt-lg-0 mt-3">
        <a href="{{ route('transaction.add') }}" class="btn btn-outline-primary btn-icon btn-block">
            <i class="material-icons">add</i> @lang('buttons.add')
        </a>
    </div>
	@endif
</div>


<form method="get" action="" id="form">
	<div class="row mb-4">
		<div class="col-8">
			<div class="input-group">
				<div class="input-group-prepend">
					<span class="input-group-text" id="search">
						<i class="material-icons">search</i>
					</span>
				</div>
				<input type="text" id="search" name="search" class="form-control" placeholder="@lang('pages.searching')" value="{{ request()->input('search') }}">
			</div>
		</div>
		<div class="col-lg-4">
			<div class="input-group">
				<div class="input-group-prepend">
					<label class="input-group-text" for="filter">
						<i class="material-icons">filter_list</i>
					</label>
				</div>
				<select class="custom-select" name="filter" id="filter" onchange="this.form.submit()">
					<option value="">@lang('pages.filter')</option>
					@foreach(config('constant.transaction.status') as $key => $value)
						<option value="{{ $key }}" {{ (request()->input('filter') != '' && request()->input('filter') == $key) ? 'selected' : '' }}>{{ __('pages.transaction.fields.status.' . $value) }}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
</form>

@if(session('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
	{!! session('success') !!}
</div>
@endif

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">@lang('pages.transaction.fields.invoice')</th>
				<th scope="col">@lang('pages.transaction.fields.branch')</th>
				<th scope="col">@lang('pages.transaction.fields.customer')</th>
				<th scope="col">@lang('pages.transaction.fields.due')</th>
				<th scope="col">@lang('pages.transaction.fields.total')</th>
				<th scope="col">@lang('pages.transaction.fields.status.name')</th>
				<th scope="col" class="column-action-2 text-center">@lang('pages.transaction.fields.action')</th>
			</tr>
		</thead>
		<tbody>
		@foreach($transactions as $transaction)
			<tr>
				<th scope="row">{{ ++$num }}</th>
				<td><a href="{{ route('transaction.detail', ['id' => $transaction->id]) }}">{{ $transaction->invoice }}</a></td>
				<td><a href="#" onclick="getBranch('{{ route('branch.detail') }}', '{{ $transaction->branch_id }}'); return false;">{{ $transaction->branch_city }}</a></td>
				<td>{{ $transaction->customer_name }}</td>
				<td>{{ $transaction->due->format('d-m-Y') }}</td>
				<td>{{ formatCurrency($transaction->total) }}</td>
				<td>{{ $transaction->status_name }}</td>
				<td class="text-center">
					@if($transaction->status == 1 && isAllowed('transaction.cancel', request()->user['level']) && isAllowed('transaction.finish', request()->user['level']))
                    <form style="display: inline" id="form-{{ $num }}" action="{{ route('transaction.cancel', ['id' => $transaction->id]) }}" method="post">
                        @method('PUT')
                        @csrf
                        <button type="button" onclick="submitForm('#form-{{ $num }}', '#cancel-{{ $num }}', '@lang('alert.confirmation.sure')');"  id="cancel-{{ $num }}" class="btn btn-sm btn-outline-danger">@lang('buttons.cancel')</button>
                    </form>

					<form style="display: inline" id="form2-{{ $num }}" action="{{ route('transaction.finish', ['id' => $transaction->id]) }}" method="post">
                        @method('PUT')
                        @csrf
                        <button type="button" onclick="submitForm('#form2-{{ $num }}', '#finish-{{ $num }}', '@lang('alert.confirmation.sure')');"  id="finish-{{ $num }}" class="btn btn-sm btn-outline-success">@lang('buttons.finish')</button>
                    </form>
					@else
					~
					@endif
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

{{ $transactions->links() }}

<!-- Modal -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">@lang('pages.branch.title') <span id="city">...</span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="form-group">
					<label for="type">@lang('pages.branch.fields.type')</label>
					<h6 id="type">...</h6>
				</div>
				<div class="form-group">
					<label for="address">@lang('pages.branch.fields.address')</label>
					<h6 id="address">...</h6>
				</div>
				<div class="form-group">
					<label for="contact">@lang('pages.branch.fields.contact')</label>
					<h6 id="contact">...</h6>
				</div>
				<div class="form-group">
					<label for="code">@lang('pages.branch.fields.code')</label>
					<h6 id="code">...</h6>
				</div>
				<div class="form-group">
					<label for="phone">@lang('pages.branch.fields.phone')</label>
					<h6 id="phone">...</h6>
				</div>
				<div class="form-group">
					<label for="fax">@lang('pages.branch.fields.fax')</label>
					<h6 id="fax">...</h6>
				</div>
				<div class="form-group">
					<label for="cellular">@lang('pages.branch.fields.cellular')</label>
					<h6 id="cellular">...</h6>
				</div>
				<div class="form-group">
					<label for="email">@lang('pages.branch.fields.email')</label>
					<h6 id="email">...</h6>
				</div>

			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
<script>
function getBranch(url, id){

	getData(
		url,
		{ id: id },
		function(response){
			var res = jQuery.parseJSON(JSON.stringify(response));
			var data = res.data;

			$('#city').html(data.city);
			$('#type').html(data.type_name);
			$('#address').html(data.address);
			$('#contact').html(data.contact);
			$('#code').html(data.code ? data.code : '-');
			$('#phone').html(data.phone ? data.phone : '-');
			$('#fax').html(data.fax ? data.fax : '-');
			$('#cellular').html(data.cellular ? data.cellular : '-');
			$('#email').html(data.email ? data.email : '-');
			$('#modal_detail').modal('show');
		}
	);

}
</script>
@endsection