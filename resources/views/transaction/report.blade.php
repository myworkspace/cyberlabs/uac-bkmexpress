@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>@lang('pages.report.title')</h2>
    </div>
</div>

<form method="get" action="{{ route('report.preview') }}" id="form" target="_blank">

    <h5 class="mb-3">@lang('pages.report.periode')</h5>

    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label for="start">@lang('pages.report.fields.start_date')</label>
                <input name="start" type="text" class="form-control datepicker" id="start" value="{{ isset($start) ? $start : date('Y-m-') . '01' }}" autocomplete="off" required>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="end">@lang('pages.report.fields.end_date')</label>
                <input name="end" type="text" class="form-control datepicker" id="end" value="{{ isset($end) ? $end : date('Y-m-d') }}" autocomplete="off" required>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">@lang('buttons.preview')</button>

</form>

@endsection

@section('javascript')
<script>
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoHide: true,
    });
</script>
@endsection