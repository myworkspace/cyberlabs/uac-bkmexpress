@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>{!! $title !!}</h2>
    </div>
    <div class="col-lg-2 text-lg-right mt-lg-0 mt-3">
        <a href="{{ url()->previous() }}" class="btn btn-outline-primary btn-icon btn-block">
            <i class="material-icons">arrow_back</i> @lang('buttons.back')
        </a>
    </div>
</div>

<form method="post" action="" id="form">

    @csrf

    <div class="row mb-3">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="invoice">@lang('pages.transaction.fields.invoice')*</label>
                <h6>{{ $invoice }}</h6>
                <input name="invoice" type="hidden" value="{{ $invoice }}" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="date">@lang('pages.transaction.fields.date')*</label>
                <h6>{{ $date }}</h6>
                <input name="date" type="hidden" value="{{ $date }}" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="branch">@lang('pages.transaction.fields.branch')</label>
                <select id="branch" class="form-control form-control-sm" name="branch" required></select>
            </div>
        </div>
        <div class="col-sm-6 offset-sm-1">
            <div class="form-group">
                <label>@lang('pages.transaction.fields.data.address')</label>
                <h6 id="branch_address_label">~</h6>
                <input name="branch_address" id="branch_address" type="hidden" required>
            </div>
        </div>
    </div>

    <div id="detail" class="table-responsive mt-3 mb-4">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">@lang('pages.transaction.fields.detail.service')</th>
                    <th scope="col">@lang('pages.transaction.fields.detail.weight')</th>
                    <th scope="col">@lang('pages.transaction.fields.detail.quantity')</th>
                    <th scope="col" class="column-action-2 text-center">@lang('pages.transaction.fields.detail.sub_total')</th>
                    <th scope="col" class="text-center">@lang('pages.transaction.fields.action')</th>
                </tr>
            </thead>
            <tbody></tbody>
            <thead>
                <tr>
                    <td colspan="6">
                        <button type="button" class="btn btn-sm btn-outline-success btn-icon" onclick="addRow();">
                            <i class="material-icons sm">add</i> 
                            @lang('buttons.add_row')
                        </button>
                    </td>
                </tr>
            </thead>
        </table>
    </div>

    <div class="row mb-3">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="due">@lang('pages.transaction.fields.due')*</label>
                <input type="text" id="due_label" class="form-control form-control-sm" required onkeyup="setTotal()">
                <input name="due" id="due" type="hidden" value="">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="total">@lang('pages.transaction.fields.total')*</label>
                <input type="text" id="total_label" class="form-control form-control-sm" required onkeyup="setTotal()">
                <input name="total" id="total" type="hidden" value="0">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="customer_name">@lang('pages.transaction.fields.customer')</label>
                <input type="text" id="customer_name" class="form-control form-control-sm mb-2" name="customer_name" placeholder="@lang('pages.transaction.fields.data.name')" required>
                <input type="text" id="customer_phone" class="form-control form-control-sm mb-2" name="customer_phone" placeholder="@lang('pages.transaction.fields.data.phone')" required>
                <textarea rows="3" id="customer_address" class="form-control form-control-sm" name="customer_address" placeholder="@lang('pages.transaction.fields.data.address')" required></textarea>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="receiver_name">@lang('pages.transaction.fields.receiver')</label>
                <input type="text" id="receiver_name" class="form-control form-control-sm mb-2" name="receiver_name" placeholder="@lang('pages.transaction.fields.data.name')" required>
                <input type="text" id="receiver_phone" class="form-control form-control-sm mb-2" name="receiver_phone" placeholder="@lang('pages.transaction.fields.data.phone')" required>
                <textarea rows="3" id="receiver_address" class="form-control form-control-sm" name="receiver_address" placeholder="@lang('pages.transaction.fields.data.address')" required></textarea>
            </div>
        </div>
    </div>

    <div class="mt-3 text-muted">@lang('pages.transaction.message.form')</div>

    <button type="submit" id="button" onclick="this.innerHTML('loading..');" class="btn btn-primary btn-block mt-3">@lang('buttons.save')</button>

</form>

@endsection

@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
        
    var row = 0;
    var count = 0;

    function addRow(){

        var element = `<tr id="row-` + row + `">
            <td>
                <select id="service-` + row + `" class="form-control form-control-sm" name="service[]" onchange="setService(` + row + `);">
                    <option>-</option>
                    @foreach($services as $service)
                    <option value="{{ $service->id }}" data-price="{{ $service->price }}" data-duration="{{ $service->duration }}" data-description="{{ $service->name }}">{{ $service->name . ' - ' . $service->duration . ' ' . __('pages.service.fields.day') . ' - ' . formatCurrency($service->price) }}</option>
                    @endforeach
                </select>
                <input type="hidden" id="description-` + row + `" name="description[]" value=0>
                <input type="hidden" id="duration-` + row + `" name="duration[]" value=0>
                <input type="hidden" id="price-` + row + `" name="price[]" value=0>
            </td>
            <td><input id="weight-` + row + `" type="number" min="1" class="form-control form-control-sm text-right" name="weight[]" value="1" onchange="calculate(` + row + `);"></td>
            <td><input id="quantity-` + row + `" type="number" min="1" class="form-control form-control-sm text-right" name="quantity[]" value="1" onchange="calculate(` + row + `);"></td>
            <td class="text-right">
                <input id="sub_total_label-` + row + `" type="text" class="form-control form-control-sm text-right" value="0" readonly>
                <input id="sub_total-` + row + `" type="hidden" name="sub_total[]" value="0">
            </td>
            <td class="text-center"><button type="button" onclick="deleteRow(` + row + `);" class="btn btn-sm btn-outline-danger">@lang('buttons.delete')</button></td>
        </tr>`;

        $('#detail table tbody').append(element);

        row++;
        count++;
    }

    function deleteRow(index){

        if(count == 1){
            alert('{{ __('alert.message.minimal', ['number' => 1, 'attribute' => __('pages.transaction.fields.detail.service')]) }}');
        } else {
            var q = confirm('{{ __('alert.confirmation.delete', ['attribute' => __('pages.transaction.fields.detail.service')]) }}');

            if(q){
                $('#row-' + index).remove();
                count--;
            }

            setTotal();
        }

    }

    function setService(index){
        var service = $('#service-' + index + ' option:selected');
        var price = service.attr('data-price');
        if(price <= 0)
            setZero(index);

        var duration = service.attr('data-duration');
        if(duration <= 0){
            setZero(index);
            return false;
        }

        var description = service.attr('data-description');
        if(description <= 0){
            setZero(index);
            return false;
        }

        $('#price-' + index).val(price);
        $('#duration-' + index).val(duration);
        $('#description-' + index).val(description);
        calculate(index);
    }

    function calculate(index){

        var price = $('#price-' + index).val();
        var duration = $('#duration-' + index).val();

        var weight = $('#weight-' + index);
        if(weight.val() <= 0)
            weight.val(1);
        
        var quantity = $('#quantity-' + index);
        if(quantity.val() <= 0)
            quantity.val(1);

        var sub_total = price * weight.val() * quantity.val();
        $('#sub_total_label-' + index).val(sub_total.formatCurrency());
        $('#sub_total-' + index).val(sub_total);

        setTotal();

    }

    function setZero(index){
        $('#sub_total_label-' + index).val(0);
        $('#sub_total-' + index).val(0);
        setTotal();
    }

    function setTotal(){
        var sub_total;
        var duration = 0;
        var total = 0;

        for(i = 0; i < row; i++){
            sub_total = $('#sub_total-' + i).val();
            if(sub_total > 0){
                total = eval(total) + eval(sub_total);

                if(eval(duration) < eval($('#duration-' + i).val()))
                    duration = $('#duration-' + i).val();
            }
        }

        if(total > 0){
            $('#total_label').val(total.formatCurrency());
        } else {
            $('#total_label').val('');
        }
        $('#total').val(total);

        // set duration
        if(duration > 0){
            var d = new Date();
            $('#due_label').val(formatDate(d.addDays(duration)), true);
            $('#due').val(formatDate(d.addDays(duration)));
        } else {
            $('#due_label').val('');
            $('#due').val('');
        }
    }

    $(document).ready(function(){

        $('#branch').select2({
            ajax: {
                url: '{{ route('branch') }}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term
                    }
                    return query;
                },
                delay: 250,
                processResults: function (response) {
                    return {
                        results:  $.map(response.data.branches.data, function (item) {
                            return {
                                text: item.city,
                                id: item.id,
                                address: item.address,
                                phone: item.phone_1,
                            }
                        })
                    };
                },

                cache: true
            }
        });

        $('#branch').on("select2:select", function(e) { 
            var data = e.params.data;
            $('#branch_address_label').html(data.address);
            $('#branch_address').html(data.address);
        });

        addRow();

    });

</script>
@endsection