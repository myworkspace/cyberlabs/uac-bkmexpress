@extends('layouts.app')

@section('body')

<div class="row mb-4">
    <div class="col-6">
        <h4>@lang('pages.report.transaction')</h4>
        <p>@lang('pages.report.periode') {{ $start }} - {{ $end }}</p>
    </div>
    <div class="col-6 text-right">
        <img src="{{ asset('images/logo-small.png') }}" />
    </div>
</div>

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">@lang('pages.transaction.fields.invoice')</th>
				<th scope="col">@lang('pages.transaction.fields.date')</th>
				<th scope="col">@lang('pages.transaction.fields.branch')</th>
				<th scope="col">@lang('pages.transaction.fields.customer')</th>
				<th scope="col">@lang('pages.transaction.fields.receiver')</th>
				<th scope="col">@lang('pages.transaction.fields.due')</th>
				<th scope="col">@lang('pages.transaction.fields.total')</th>
				<th scope="col">@lang('pages.transaction.fields.status.name')</th>
			</tr>
		</thead>
		<tbody>
		@php
			$total = 0;
		@endphp
        @foreach($transactions as $transaction)
			<tr>
				<th scope="row">{{ ++$num }}</th>
				<td>{{ $transaction->invoice }}</td>
				<td>{{ $transaction->created_at->format('d-m-Y') }}</td>
				<td>
                    <b>{{ $transaction->branch_city }}</b><br/>
                    {{ $transaction->branch_phone }}<br/>
                    {{ $transaction->branch_address }}<br />
                </td>
				<td>
                    <b>{{ $transaction->customer_name }}</b><br />
                    {{ $transaction->customer_phone }}<br />
                    {{ $transaction->customer_address }}<br />
                </td>
				<td>
                    <b>{{ $transaction->receiver_name }}</b><br />
                    {{ $transaction->receiver_phone }}<br />
                    {{ $transaction->receiver_address }}<br />
                </td>
				<td>{{ $transaction->due->format('d-m-Y') }}</td>
				<td>{{ formatCurrency($transaction->total) }}</td>
				<td>{{ $transaction->status_name }}</td>
			</tr>
			@php
				$total = $total + $transaction->total;
			@endphp
		@endforeach
		</tbody>
		<tfooter>
			<tr>
				<th colspan="7">@lang('pages.transaction.fields.sub_total')</th>
				<th>{{ formatCurrency($total) }}</th>
				<th></th>
			</tr>
			<tr>
				<th colspan="7">@lang('pages.transaction.fields.grand_total')</th>
				<th>{{ formatCurrency($grand_total) }}</th>
				<th></th>
			</tr>
		</tfooter>
    </table>
</div>

{{ $transactions->links() }}

@endsection