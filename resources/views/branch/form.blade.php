@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>{!! $title !!}</h2>
    </div>
    <div class="col-lg-2 text-lg-right mt-lg-0 mt-3">
        <a href="{{ url()->previous() }}" class="btn btn-outline-primary btn-icon btn-block">
            <i class="material-icons">arrow_back</i> @lang('buttons.back')
        </a>
    </div>
</div>

<form method="post" action="" id="form">

    @csrf

    @if(isset($edit))
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="city">@lang('pages.branch.fields.city')</label>
                <input name="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" id="city" value="{{ old('city') ? old('city') : (isset($branch->city) ? $branch->city : '') }}" required>
                @if ($errors->has('city'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="type">@lang('pages.branch.fields.type')</label>
                <select id="type" name="type" class="form-control">
                    @foreach($types as $key=>$type)
                        <option {{ $key == $type_id ? 'selected' : '' }} value="{{ $key }}">{{ $type }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="address">@lang('pages.branch.fields.address')</label>
                <textarea name="address" rows="3" id="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" required>{{ old('address') ? old('address') : (isset($branch->address) ? $branch->address : '') }}</textarea>
                @if ($errors->has('address'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="contact">@lang('pages.branch.fields.contact')</label>
                <input name="contact" type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" id="contact" value="{{ old('contact') ? old('contact') : (isset($branch->contact) ? $branch->contact : '') }}" required>
                @if ($errors->has('contact'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact') }}</strong>
                </span>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="code">@lang('pages.branch.fields.code')</label>
                        <input name="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" id="code" value="{{ old('fax') ? old('code') : (isset($branch->code) ? $branch->code : '') }}" required>
                        @if ($errors->has('code'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('code') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="phone_2">@lang('pages.branch.fields.phone')</label>
                        <input name="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" value="{{ old('phone') ? old('phone') : (isset($branch->phone) ? $branch->phone : '') }}" required>
                        @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="fax">@lang('pages.branch.fields.fax')</label>
                        <input name="fax" type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : '' }}" id="fax" value="{{ old('fax') ? old('fax') : (isset($branch->fax) ? $branch->fax : '') }}" required>
                        @if ($errors->has('fax'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('fax') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="cellular">@lang('pages.branch.fields.cellular')</label>
                        <input name="cellular" type="text" class="form-control{{ $errors->has('cellular') ? ' is-invalid' : '' }}" id="cellular" value="{{ old('cellular') ? old('cellular') : (isset($branch->cellular) ? $branch->cellular : '') }}">
                        @if ($errors->has('cellular'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('cellular') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="email">@lang('pages.branch.fields.email')</label>
                <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" value="{{ old('email') ? old('email') : (isset($branch->email) ? $branch->email : '') }}" required>
                @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <button type="button" onclick="submitForm();" id="button" class="btn btn-primary mt-4">{{ isset($edit) ? __('buttons.update') : __('buttons.save') }}</button>
        </div>
    </div>

</form>

@endsection