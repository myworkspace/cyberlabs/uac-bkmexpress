@extends('layouts.app')

@section('body')

<div class="row">
    <div class="col-lg-3">
    
    <nav class="navbar navbar-expand-lg navbar-light flex-lg-column p-0 text-left">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('images/logo-small.png') }}" class="logo" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse mt-3" id="navbarNav">
            <ul class="navbar-nav flex-lg-column">
                <li class="nav-item">
                    <small class="nav-link text-muted">@lang('menus.management')</small>
                </li>

                @if(isAllowed('branch', request()->user['level']))
                <li class="nav-item">
                    <a class="nav-link {{ (request()->segment(1) == 'branch') ? 'active' : '' }}" href="{{ route('branch') }}">@lang('menus.branch')</a>
                </li>
                @endif

                @if(isAllowed('service', request()->user['level']))
                <li class="nav-item">
                    <a class="nav-link {{ (request()->segment(1) == 'service') ? 'active' : '' }}" href="{{ route('service') }}">@lang('menus.service')</a>
                </li>
                @endif
                
                <li class="nav-item">
                    <a class="nav-link {{ (request()->segment(1) == 'transaction') ? 'active' : '' }}" href="{{ route('transaction') }}">@lang('menus.transaction')</a>
                </li>

                @if(isAllowed('report', request()->user['level']))
                <li class="nav-item">
                    <a class="nav-link {{ (request()->segment(1) == 'report') ? 'active' : '' }}" href="{{ route('report') }}">@lang('menus.report')</a>
                </li>
                @endif

                <li class="nav-item mt-4">
                    <small class="nav-link text-muted text-uppercase">{{ request()->user['name'] }}</small>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="return confirm('@lang('alert.confirmation.logout')'); return false;">@lang('menus.logout')</a>
                </li>
            </ul>
        </div>
    </nav>

    </div>
    <div class="col-lg-9 mt-4 mt-lg-0">
        @yield('content')
    </div>
</div>

@endsection
