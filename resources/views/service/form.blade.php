@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>{!! $title !!}</h2>
    </div>
    <div class="col-lg-2 text-lg-right mt-lg-0 mt-3">
        <a href="{{ url()->previous() }}" class="btn btn-outline-primary btn-icon btn-block">
            <i class="material-icons">arrow_back</i> @lang('buttons.back')
        </a>
    </div>
</div>

<form method="post" action="" id="form">

    @csrf

    @if(isset($edit))
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="name">@lang('pages.service.fields.name')</label>
                <input name="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" value="{{ old('name') ? old('name') : (isset($service->name) ? $service->name : '') }}" required>
                @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="price">@lang('pages.service.fields.price')</label>
                <input name="price" type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="price" value="{{ old('price') ? old('price') : (isset($service->price) ? $service->price : '') }}" required>
                @if ($errors->has('price'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="duration">@lang('pages.service.fields.duration')</label>
                <input name="duration" type="text" class="form-control{{ $errors->has('duration') ? ' is-invalid' : '' }}" id="duration" value="{{ old('duration') ? old('duration') : (isset($service->duration) ? $service->duration : '') }}" required>
                @if ($errors->has('duration'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('duration') }}</strong>
                </span>
                @endif
            </div>

            <button type="button" onclick="submitForm();" id="button" class="btn btn-primary mt-4">{{ isset($edit) ? __('buttons.update') : __('buttons.save') }}</button>
        </div>
    </div>

</form>

@endsection