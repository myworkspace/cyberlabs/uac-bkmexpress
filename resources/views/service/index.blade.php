@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>@lang('pages.service.title')</h2>
    </div>
	@if(isAllowed('service.add', request()->user['level']))
    <div class="col-lg-2 text-lg-right mt-lg-0 mt-3">
        <a href="{{ route('service.add') }}" class="btn btn-outline-primary btn-icon btn-block">
            <i class="material-icons">add</i> @lang('buttons.add')
        </a>
    </div>
	@endif
</div>

@if(session('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
	{!! session('success') !!}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
	{!! session('error') !!}
</div>
@endif

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">@lang('pages.service.fields.name')</th>
				<th scope="col">@lang('pages.service.fields.price')</th>
				<th scope="col">@lang('pages.service.fields.duration')</th>
				<th scope="col" class="column-action-2 text-center">@lang('pages.service.fields.action')</th>
			</tr>
		</thead>
		<tbody>
		@foreach($services as $service)
			<tr>
				<th scope="row">{{ ++$num }}</th>
				<td>{{ $service->name }}</td>
				<td>{{ formatCurrency($service->price) }}</td>
				<td>{{ $service->duration }}</td>
				<td class="text-center">

					@if(isAllowed('service.edit', request()->user['level']))
                    <a href="{{ route('service.edit', ['id' => $service->id]) }}" class="btn btn-sm btn-outline-success">@lang('buttons.edit')</a>
					@else
					~
					@endif

					@if(isAllowed('service.delete', request()->user['level']))
                    <form style="display: inline" id="form-{{ $num }}" action="{{ route('service.delete', ['id' => $service->id]) }}" method="post">
                        @method('DELETE')
                        @csrf
                        <button type="button" onclick="submitForm('#form-{{ $num }}', '#delete-{{ $num }}', '@lang('alert.confirmation.delete', ['attribute' => $service->name])');"  id="delete-{{ $num }}" class="btn btn-sm btn-outline-primary">@lang('buttons.delete')</button>
                    </form>
					@endif

				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

@endsection
