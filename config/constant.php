<?php

return [
    'branch' => [
        'types' => [
            1 => 'Kantor Perwakilan',
            2 => 'Agen Utama',
            3 => 'Sub Agen',
        ],
    ],
    
    'transaction' => [
        'status' => [
            0 => 'cancelled',
            1 => 'active',
            2 => 'finished',
        ]
    ],
];